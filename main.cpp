#include <QDebug>
#include <QGuiApplication>
#include <QLibraryInfo>
#include <QQmlApplicationEngine>
#include <QTranslator>

void installTranslations(QCoreApplication &app)
{
    static QTranslator qtTranslator;
    static QTranslator appTranslator;

    auto localeName = QLocale::system().name();

    qtTranslator.load("qt_" + localeName,
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    appTranslator.load("translations/chasopys_" + localeName);
    //appTranslator.load("translations/chasopys_uk");

    app.installTranslator(&qtTranslator);
    app.installTranslator(&appTranslator);
}

int main(int argc, char **argv)
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    installTranslations(app);

    app.setApplicationDisplayName(QCoreApplication::tr("Chasopys"));
    app.setApplicationName(QCoreApplication::tr("Chasopys"));
    app.setOrganizationName(QCoreApplication::tr("Oleksandr Manenko"));

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/ui/Main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
