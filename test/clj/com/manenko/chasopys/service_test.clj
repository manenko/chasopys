(ns com.manenko.chasopys.service-test
  (:require [buddy.core.codecs            :as    codecs]
            [buddy.core.codecs.base64     :as    base64]
            [clojure.test                 :refer :all]
            [com.manenko.chasopys.service :as    service]
            [io.pedestal.http             :as    http]
            [io.pedestal.test             :refer :all]))


(def service
  (::http/service-fn (http/create-servlet service/service)))


(defn make-headers
  [username password]
  (let [b64-encoded (base64/encode (str username ":" password))]
    {"Authorization" (str "Basic " (codecs/bytes->str b64-encoded))}))


(defn GET
  [service & opts]
  (apply response-for service :get opts))


(deftest home-test
  (testing "Anonymous user"
    (is (= "Hello anonymous"
           (:body (GET service "/")))))
  (testing "Authenticated user"
    (is (= "Hello Oleksandr Manenko"
           (:body (GET service "/" :headers (make-headers "omanenko" "secret")))))))


(deftest admin-test
  (are [status username password _] (= status
                                       (:status (if username
                                                  (GET service "/admin"
                                                       :headers (make-headers username password))
                                                  (GET service "/admin"))))
    401  nil         nil       "Unauthenticated access is unauthorized."
    403  "omanenko"  "secret"  "Unauthorized access is forbidden."
    200  "thrawn"    "syndic"  "Authorized access is allowed."))
