(defproject chasopys "0.1.0-SNAPSHOT"
  :url               "https://gitlab.com/manenko/chasopys"
  :scm               {:url "https://gitlab.com/manenko/chasopys"}
  :license           {"Apache License, Version 2.0" "https://www.apache.org/licenses/LICENSE-2.0.txt"}
  :dependencies      [[io.pedestal/pedestal.service       "0.5.5"]
                      [io.pedestal/pedestal.jetty         "0.5.5"]
                      [buddy/buddy-auth                   "2.1.0"           :exclusions [cheshire]]
                      [ch.qos.logback/logback-classic     "1.2.3"           :exclusions [org.slf4j/slf4j-api]]
                      [org.slf4j/jul-to-slf4j             "1.7.25"]
                      [org.slf4j/jcl-over-slf4j           "1.7.25"]
                      [org.slf4j/log4j-over-slf4j         "1.7.25"]
                      [org.clojure/clojure                "1.10.0"          :scope       "provided"]]
  :min-lein-version  "2.0.0"
  :resource-paths    ["config"   "resources"]
  :source-paths      ["src/clj"  "src/cljc"  "src/cljs"]
  :test-paths        ["test/clj" "test/cljc" "test/cljs"]
  :profiles          {:dev     {:aliases      {"run-dev" ["trampoline" "run" "-m" "com.manenko.chasopys.server/run-dev"]}
                                :dependencies [[io.pedestal/pedestal.service-tools "0.5.5"]]}
                      :uberjar {:aot [com.manenko.chasopys.server]}}
  :main              ^{:skip-aot true} com.manenko.chasopys.server)
