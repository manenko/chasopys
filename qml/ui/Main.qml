import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.3

import "."

ApplicationWindow {
    visible: true
    width: 640
    height: 396
    minimumWidth: 640/2
    minimumHeight: 396/2
    title: Qt.application.displayName

    Rectangle {
        id: rectangle
        anchors.fill: parent
        anchors.margins: Theme.rootElementMargin
        border.width: 0
        color: "transparent"

        TextField {
            implicitWidth: width
            anchors.left: parent.left
            anchors.right: parent.right

            placeholderText: qsTr("What's up?")
        }
    }
}
