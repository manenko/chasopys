(ns com.manenko.chasopys.server
  (:gen-class)
  (:require [io.pedestal.http             :as server]
            [io.pedestal.http.route       :as route]
            [com.manenko.chasopys.service :as service]))


(defonce runnable-service (server/create-server service/service))


(defn run-dev
  "The entry-point for 'lein run-dev'"
  [& args]
  (println "\nCreating your [DEV] server...")
  (-> service/service ;; Start with production configuration
      (merge {:env :dev
              ;; Do not block thread that starts web server
              ::server/join? false
              ;; Routes can be a function that resolve routes, we can use this
              ;; to set the routes to be reloadable
              ::server/routes #(route/expand-routes (deref #'service/routes))
              ;; All origins are allowed in dev mode
              ::server/allowed-origins {:creds true :allowed-origins (constantly true)}})
      ;; Wire up interceptor chains
      server/default-interceptors
      server/dev-interceptors
      server/create-server
      server/start))


(defn -main
  "The entry-point for 'lein run'"
  [& args]
  (println "\nCreating your server...")
  (server/start runnable-service))
