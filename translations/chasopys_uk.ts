<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>Main</name>
    <message>
        <location filename="../qml/ui/Main.qml" line="25"/>
        <location filename="../main.cpp" line="26"/>
        <source>What&apos;s up?</source>
        <translation>Як справи?</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <location filename="../main.cpp" line="25"/>
        <location filename="../main.cpp" line="35"/>
        <location filename="../main.cpp" line="36"/>
        <source>Chasopys</source>
        <translation>Часопис</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="37"/>
        <source>Oleksandr Manenko</source>
        <translation>Олександр Маненко</translation>
    </message>
</context>
</TS>
